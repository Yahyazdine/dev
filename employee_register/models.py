from django.db import models

# Create your models here.

class Position(models.Model):
    title = models.CharField(max_length=50)
    id = models.IntegerField(primary_key=True)
    def __str__(self):
        return self.title

class Employee(models.Model):

    fullname = models.CharField(max_length=100)
    emp_code = models.CharField(max_length=3)
    mobile= models.CharField(max_length=15)
    position= models.ForeignKey(Position,on_delete=models.CASCADE)
    @property
    def check_code(self):
        code_emp = int(self.emp_code)
        return ( code_emp <= 999 and code_emp > 0 ) 

pos1 = Position(title='Developer', id=1)
pos1.save()

pos2 = Position(title='IT Technician', id=2)
pos2.save()

pos3 = Position(title='QA', id=3)
pos3.save()

pos4 = Position(title='Operator', id=4)
pos4.save()

pos5 = Position(title='Accountant', id=5)
pos5.save()
pos6 = Position(title='Accountant', id=6)
pos6.save()
    
